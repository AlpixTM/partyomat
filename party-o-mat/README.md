# Profile-O-Mat 
[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

Give us your Twitter name, and we'll tell you who to vote for! (Or at least which party your tweets are most similar to). Profile-O-Mat is an artifial neural network predicting your political orientation based on your choice of words. You can either analyze single words, sentences or your twitter account based on the last 200 tweets. Included in this repository is an allready trained ANN and the word-bag definition allowing you to predict with an overall success rate of about 75% (5 times higher than guessing).

### Getting started

You can find our dependencies as well as the installation instructions [here](INSTALL.md).
At the moment there is no server hosting Profile-O-Mat (as far as we know).