FORK OF [GITHUB VERSION](https://github.com/AlpixTM/partyomat)!
  
This Project is based on [Profile-O-Mat](https://bitbucket.org/profileomat/) by [profileomat](https://bitbucket.org/profileomat/).  
It contains the repos [Profile-O-Mat](https://bitbucket.org/profileomat/profile-o-mat), [Android-App](https://bitbucket.org/profileomat/android-app), [twitterwall](https://bitbucket.org/profileomat/twitterwall) and [Discord-Bot](https://bitbucket.org/profileomat/discord-bot) written by 
[Levy Ehrstein ["StoneInc"]](https://bitbucket.org/StoneInc/),
[Moritz Gehlhaar["geoextra"]](https://bitbucket.org/geoextra/),
[derNiklaas](https://bitbucket.org/derNiklaas/),
[be7a](https://bitbucket.org/be7a/),
[Adrian["uberardy"]](https://bitbucket.org/uberardy/),
[Torben Wetter](https://bitbucket.org/TorbenWetter/) and
[SparxDev](https://bitbucket.org/SparxDev/).
  
[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-nc-sa/4.0/)